# .bashrc

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

pfetch
alias em="emacsclient -c -a ''"
alias emacsync='~/.emacs.d/bin/doom sync'
alias xql='xbps-query --regex -Rs '^linux[0-9.]+-[0-9._]+''
alias xrr='sudo xbps-remove -R'
alias xre='sudo xbps-remove'
alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '
